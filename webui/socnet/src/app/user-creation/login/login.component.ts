import {Component, OnInit, OnDestroy, HostListener} from '@angular/core';
import {Router} from '@angular/router';
import {LoginService} from 'src/app/services/login.service';
import * as $ from 'jquery';
import {RegisterUser} from 'src/app/entities/RegisterUser';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {

  isLogin = true;
  model: any = {};
  errMsg: string = '';
  containerEl: any;

  constructor(
    private router: Router,
    private loginService: LoginService,
    private userService:UserService) {
  }

  ngOnInit() {
    $('#top-container').css({'width': '100%', 'height': '100%'});
    // reset login status
    this.loginService.logout(false);
  }

  ngOnDestroy(): void {
    $('#top-container').css({'width': '90%', 'height': 'calc(100vh - 95px)'});
  }

  login() {
    this.loginService.getToken(this.model.username, this.model.password)
      .subscribe(resp => {
        console.log("response",resp);
          if (resp.user === undefined || resp.user.token === undefined || resp.user.token === 'INVALID') {
            this.errMsg = 'Неверное имя пользователя или пароль';
            return;
          }
          this.navigate(resp.landingPage);

        },
        errResponse => {
          switch (errResponse.status) {
            case 401:
            case 403:
              this.errMsg = 'Неверное имя пользователя или пароль';
              break;
            case 404:
              this.errMsg = 'Страница не найдена';
              break;
            case 408:
              this.errMsg = 'Истекло вря ожидания';
              break;
            case 500:
              this.errMsg = 'Internal Server Error';
              break;
            default:
              this.errMsg = 'Server Error';
          }
        }
      );
  }

  navigate(landingPage: any){
    this.userService.getLoginUser().subscribe(res=>{
        this.router.navigate([landingPage]);
    });
  }

  onSignUp() {
    this.router.navigate(['signup']);
  }

  goReg() {
    if (this.isLogin) {
      this.isLogin = false;
      this.model.username = '';
      this.model.passowrd = '';
    } else {
      this.isLogin = true;
    }

  }

  registrartion() {
    const regUser = new RegisterUser();
    regUser.userId=this.model.username;
    regUser.password = this.model.password;
    regUser.confirmPassword = this.model.password;
    this.loginService.register(regUser).subscribe(answer => {
      console.log(answer);
    });
  }

  @HostListener('window:keyup', ['$event'])
  keyEvent(event: KeyboardEvent) {
    if (event.keyCode === 13) {
      if (this.isLogin == true){
        this.login();
      }
      else {
        this.registrartion();
      }
    }
  }
}
