import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { UserCreationRoutingModule } from './user-creation-routing.module';
import { LoginComponent } from './login/login.component';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { RegistrationComponent } from './registration/registration.component';
import {MatSnackBarModule} from '@angular/material/snack-bar';


@NgModule({
  declarations: [LoginComponent,RegistrationComponent],
  imports: [
    UserCreationRoutingModule,
    CommonModule,
    FormsModule,
    MatSnackBarModule
  ]
})
export class UserCreationModule { }
