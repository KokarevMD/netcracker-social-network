import { Component, OnInit, OnDestroy, HostListener } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from 'src/app/services/login.service';
import * as $ from 'jquery';
import { RegisterUser } from 'src/app/entities/RegisterUser';
import { UserService } from 'src/app/services/user.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent implements OnInit, OnDestroy {

  isLogin = true;
  model: any = {};
  errMsg: string = '';
  containerEl:any;

  constructor(
    private router: Router,
    private loginService: LoginService,
    private userService: UserService,
    private snackBar:MatSnackBar) {
  }

  ngOnInit() {
    $('#top-container').css({ 'width': '100%', 'height': '100%' });
    // reset login status
    this.loginService.logout(false);
  }

  ngOnDestroy(): void {
    $('#top-container').css({ 'width': '90%', 'height': 'calc(100vh - 95px)' });
  }
  navigate(landingPage: any) {
    this.userService.getLoginUser().subscribe(res => {
      this.router.navigate([landingPage]);
    });
  }

  onSignUp() {
    this.router.navigate(['signup']);
  }
  registrartion() {
    const regUser = new RegisterUser();
    regUser.userId = this.model.username;
    regUser.name = this.model.name;
    regUser.surname = this.model.surname;
    regUser.password = this.model.password;
    regUser.confirmPassword = this.model.confirmPassword;
    if(regUser.confirmPassword!==regUser.password){
      this.snackBar.open('Comfirmed password is wrong', 'Ok :(', {
        duration: 3000
      });
    } else{
      this.loginService.register(regUser).subscribe(answer => {
        this.router.navigate([`/login`])
      });
    }
    
  }

  @HostListener('window:keyup', ['$event'])
  keyEvent(event: KeyboardEvent) {
    if (event.keyCode === 13) {
      this.registrartion();
    }
  }
}
