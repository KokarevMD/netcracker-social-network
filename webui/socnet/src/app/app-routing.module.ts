import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', loadChildren: () => import ('./home/home.module').then(m=>m.HomeModule)},
  { path: 'groups', loadChildren: () => import ('./groups/groups.module').then(m=>m.GroupsModule)},
  { path: 'login', loadChildren: () => import('./user-creation/user-creation.module').then(m => m.UserCreationModule) },
  { path:'profile', loadChildren: () => import('./profile/profile.module').then(m=>m.ProfileModule)},
  { path:'friends', loadChildren: () => import('./friends/friends.module').then(m=>m.FriendsModule)}

];
  // { path: 'logout', component: LogoutComponent },];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
