import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GroupsComponent } from './groups.component';
import { MyGroupsComponent } from './my-groups/my-groups.component';


const routes: Routes = [{ path: 'join', component: GroupsComponent },
                        { path: 'my', component: MyGroupsComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GroupsRoutingModule { }
