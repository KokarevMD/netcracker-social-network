import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Group } from 'src/app/entities/Group';
import { User } from 'src/app/entities/User';
import { GroupService } from 'src/app/services/group.service';
import { UserService } from 'src/app/services/user.service';
import { AddGroupComponent } from '../add-group/add-group.component';
import { JoinGroupComponent } from '../join-group/join-group.component';
import { DeleteGroupComponent } from './delete-group/delete-group.component';
import { LeaveGroupComponent } from './leave-group/leave-group.component';

@Component({
  selector: 'app-my-groups',
  templateUrl: './my-groups.component.html',
  styleUrls: ['./my-groups.component.css']
})
export class MyGroupsComponent implements OnInit {
  user!: User;
  groups: Group[] = new Array();
  displayedColumns: string[] = ['groupName', 'creator', 'join'];
  dataSource!: MatTableDataSource<any>;
  constructor(private groupService: GroupService, private userService: UserService, private dialogRef: MatDialog) { }

  @ViewChild(MatSort, { static: true }) sort!: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator!: MatPaginator;

  ngOnInit(): void {
    this.userService.getLoginUser().subscribe(user => {
      this.user = user;
      this.getGroups(this.user);
    });
  }
  getGroups(user: User) {
    this.groupService.getGroupsForUser(user.userId).subscribe(res => { this.groups = res; console.log(res); this.tableInit(res)});
  }

  tableInit(groups: Group[]): void {
    this.dataSource = new MatTableDataSource<Group>(groups);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(event: Event): void {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  openDeleteGroup(group: Group) {
    const dialog = this.dialogRef.open(DeleteGroupComponent, {
      width: '250px',
      data: {
        group
      }
    });

    dialog.afterClosed().subscribe(res => {
      if (res !== undefined) {
        let i = this.groups.indexOf(group);
        this.groups.splice(i, 1);
        this.tableInit(this.groups);
      }
    });
  }

  openLeaveGroup(group: Group) {
    const dialog = this.dialogRef.open(LeaveGroupComponent, {
      width: '250px',
      data: {
        group
      }
    });

    dialog.afterClosed().subscribe(res => {
      if (res !== undefined) {
        let i = this.groups.indexOf(group);
        this.groups.splice(i, 1);
        this.tableInit(this.groups);
      }
    });
  }
}