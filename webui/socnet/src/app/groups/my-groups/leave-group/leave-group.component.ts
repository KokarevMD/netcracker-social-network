import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Group } from 'src/app/entities/Group';
import { GroupService } from 'src/app/services/group.service';
import { JoinGroupComponent } from '../../join-group/join-group.component';

@Component({
  selector: 'app-leave-group',
  templateUrl: './leave-group.component.html',
  styleUrls: ['./leave-group.component.css']
})
export class LeaveGroupComponent implements OnInit {
  group!:Group;
  constructor(public dialogRef: MatDialogRef<LeaveGroupComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,private groupService:GroupService) { }

  ngOnInit(): void {
    this.group = this.data.group;
  }
  onYesClick() {
    this.groupService.leaveGroup(this.group.id).subscribe(res=>{});
    this.dialogRef.close(this.group);
  }

  onNoClick() {
    this.dialogRef.close();
  }

}
