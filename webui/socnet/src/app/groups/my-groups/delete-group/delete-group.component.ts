import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Group } from 'src/app/entities/Group';
import { GroupService } from 'src/app/services/group.service';
import { JoinGroupComponent } from '../../join-group/join-group.component';

@Component({
  selector: 'app-delete-group',
  templateUrl: './delete-group.component.html',
  styleUrls: ['./delete-group.component.css']
})
export class DeleteGroupComponent implements OnInit {
  group!:Group;
  constructor(public dialogRef: MatDialogRef<DeleteGroupComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,private groupService:GroupService) { }

  ngOnInit(): void {
    this.group = this.data.group;
  }
  onYesClick() {
    this.groupService.deleteGroup(this.group.id).subscribe(res=>{});
    this.dialogRef.close(this.group);
  }

  onNoClick() {
    this.dialogRef.close();
  }
}
