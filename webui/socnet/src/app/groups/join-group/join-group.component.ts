import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Group } from 'src/app/entities/Group';
import { User } from 'src/app/entities/User';
import { AddRequestComponent } from 'src/app/friends/add-request/add-request.component';
import { GroupService } from 'src/app/services/group.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-join-group',
  templateUrl: './join-group.component.html',
  styleUrls: ['./join-group.component.css']
})
export class JoinGroupComponent implements OnInit {
  group!:Group;
  constructor(public dialogRef: MatDialogRef<JoinGroupComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,private groupService:GroupService) { }

  ngOnInit(): void {
    this.group = this.data.group;
  }
  onYesClick() {
    this.groupService.joinGroup(this.group.id).subscribe(res=>{});
    this.dialogRef.close(this.group);
  }

  onNoClick() {
    this.dialogRef.close();
  }
}
