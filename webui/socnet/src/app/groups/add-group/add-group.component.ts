import { Component, Inject, OnInit, TestabilityRegistry } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Group } from 'src/app/entities/Group';
import { User } from 'src/app/entities/User';
import { AddRequestComponent } from 'src/app/friends/add-request/add-request.component';
import { GroupService } from 'src/app/services/group.service';

@Component({
  selector: 'app-add-group',
  templateUrl: './add-group.component.html',
  styleUrls: ['./add-group.component.css']
})
export class AddGroupComponent implements OnInit {
  name!: string;
  constructor(public dialogRef: MatDialogRef<AddGroupComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, private groupService: GroupService) { }

  ngOnInit(): void {
  }

  onYesClick() {
    let group: Group = new Group();
    group.name = this.name;
    this.groupService.addGroup(group).subscribe(res => { });
    this.dialogRef.close();
  }

  onNoClick() {
    this.dialogRef.close();
  }

}
