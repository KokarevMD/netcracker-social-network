import { NgModule } from '@angular/core';
import {MatRadioModule} from '@angular/material/radio';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { FormsModule } from '@angular/forms';
import { MatDialogModule } from '@angular/material/dialog';
import { CommonModule } from '@angular/common';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { GroupsRoutingModule } from './groups-routing.module';
import { MatTableModule } from '@angular/material/table';
import { GroupsComponent } from './groups.component';
import { JoinGroupComponent } from './join-group/join-group.component';
import { AddGroupComponent } from './add-group/add-group.component';
import { MyGroupsComponent } from './my-groups/my-groups.component';
import { DeleteGroupComponent } from './my-groups/delete-group/delete-group.component';
import { LeaveGroupComponent } from './my-groups/leave-group/leave-group.component';



@NgModule({
  declarations: [
    GroupsComponent,
    JoinGroupComponent,
    AddGroupComponent,
    MyGroupsComponent,
    DeleteGroupComponent,
    LeaveGroupComponent
  ],
  imports: [
    CommonModule,
    GroupsRoutingModule,
    MatDialogModule,
    MatIconModule,
    MatInputModule,
    MatRadioModule,
    MatFormFieldModule,
    MatTableModule,
    FormsModule,
    MatRadioModule,
    MatSnackBarModule,
    MatProgressSpinnerModule
  ]
})
export class GroupsModule { }
