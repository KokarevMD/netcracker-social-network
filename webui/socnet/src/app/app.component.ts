import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from './services/user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  title = 'NC SocNet';
  constructor(private userService:UserService,private router:Router){}
  ngOnInit(): void {
    this.userService.getLoginUser().subscribe(user => {
      if (user == undefined) this.router.navigateByUrl(`/login`);
    },err=>{
      console.log(err);
      if(err.status==404){this.router.navigateByUrl(`/login`);}
    });
  }
  
}
