import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LogoutComponent } from './components/logout/logout.component';
import { MenuComponent } from './components/menu/menu.component';
import { UserWidgetComponent } from './components/user-widget/user-widget.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ApiRequestService } from './services/api-request.service';
import { LoginService } from './services/login.service';
import { UserInfoService } from './services/user-info.service';
import { AppConfig } from './app-config';
import { UserService } from './services/user.service';
import { ProfileComponent } from './profile/profile.component';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import {MatRadioModule} from '@angular/material/radio';
import { MatFormFieldModule } from '@angular/material/form-field';
import { FormsModule } from '@angular/forms';
import { FriendsComponent } from './friends/friends.component';
import { MatMenuModule } from '@angular/material/menu';
import { PostService } from './services/post.service';
import { GroupsComponent } from './groups/groups.component';
import { GroupService } from './services/group.service';



@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    UserWidgetComponent,
    LogoutComponent,
    ProfileComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    //--imports
    MatIconModule,
    MatInputModule,
    MatRadioModule,
    MatMenuModule,
    MatFormFieldModule,
    FormsModule
  ],
  providers: [
    UserInfoService,
    LoginService,
    ApiRequestService,
    AppConfig,
    UserService,
    PostService,
    GroupService],
  bootstrap: [AppComponent]
})
export class AppModule { }
