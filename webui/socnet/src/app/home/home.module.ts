import { NgModule } from '@angular/core';

import { HomeRoutingModule } from './home-routing.module';
import { HomePageComponent } from './home/home-page.component';
import { MatDialogModule } from '@angular/material/dialog';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { AddPostComponent } from './add-post/add-post.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { FormsModule } from '@angular/forms';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatRadioModule } from '@angular/material/radio';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { NgxImageSwiperModule } from 'ngx-image-swiper';
import { PostCardComponent } from '../components/post-card/post-card.component';




@NgModule({
  declarations: [HomePageComponent, AddPostComponent,PostCardComponent],
  imports: [
    HomeRoutingModule,
    MatDialogModule,
    CommonModule,
    MatFormFieldModule,
    CommonModule,
    MatDialogModule,
    MatIconModule,
    MatInputModule,
    MatRadioModule,
    MatFormFieldModule,
    FormsModule,
    MatRadioModule,
    MatSnackBarModule,
    MatProgressSpinnerModule,
    NgxImageSwiperModule
  ]
})
export class HomeModule { }

