import { getSafePropertyAccessString } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import * as $ from 'jquery';
import { NgxSwiperConfig } from 'ngx-image-swiper';
import { runInThisContext } from 'node:vm';
import { Post } from 'src/app/entities/Post';
import { PostImage } from 'src/app/entities/PostImage';
import { User } from 'src/app/entities/User';
import { PostService } from 'src/app/services/post.service';
import { UserInfoService } from 'src/app/services/user-info.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit {
  user!: User;
  posts: Post[] = new Array();
  constructor(private userService: UserService, private userInfoService: UserInfoService, private postService: PostService, private router: Router) {
  }

  ngOnInit(): void {
    this.userService.getLoginUser().subscribe(res => {
      this.getPosts(res);
    });
    this.setImages();
  }
  getPosts(user: User) {
    this.postService.getFriendsPosts(user.userId).subscribe(res => {
      this.posts = res;
      console.log('posts', res);
    });
  }

  setImages() {
    this.posts.map(post => this.setImage(post));
  }

  setImage(post: Post) {
    console.log(post.id);
    if (post?.images[0]?.base64 !== undefined || post.images[0] !== undefined) {
      $('.' + post.id).css('background-image',
        'url(data:image/gif;base64,' + post.images[0].base64 + ')');
      $('.' + post.id).css('background-size',
        'contain');
    }
  }
}
