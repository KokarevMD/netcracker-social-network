import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddPostComponent } from './add-post/add-post.component';
import { HomePageComponent } from './home/home-page.component';




const routes: Routes = [
  { path: '', component: HomePageComponent },
  { path: 'post/add', component: AddPostComponent },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
