import { Component, OnInit } from '@angular/core';
import { Post } from 'src/app/entities/Post';
import { NgxSwiperConfig } from 'ngx-image-swiper';
import { PostImage } from 'src/app/entities/PostImage';
import { User } from 'src/app/entities/User';
import { UserService } from 'src/app/services/user.service';
import { Router } from '@angular/router';
import { PostService } from 'src/app/services/post.service';
import { ThrowStmt } from '@angular/compiler';
import * as $ from 'jquery';

@Component({
  selector: 'app-add-post',
  templateUrl: './add-post.component.html',
  styleUrls: ['./add-post.component.css']
})
export class AddPostComponent implements OnInit {
  user!: User;
  post: Post = new Post(0);
  postImages: [string] = [''];
  isImgShow!: boolean;
  swiperConfig: NgxSwiperConfig = {
    navigation: true,
    navigationPlacement: 'inside',
    pagination: false,
    paginationPlacement: 'inside',
    loop: true,
    imgBackgroundSize: 'contain',

  };
  constructor(
    private userService: UserService,
    private router: Router,
    private postService: PostService) { }

  ngOnInit(): void {
    this.post.images = new Array();
    this.post.postText = '';
    this.isImgShow = false;
  }

  setImages(imgs: PostImage[]) {
      this.postImages.splice(0,this.postImages.length);

    for (let i = 0; i < imgs.length; i++) {
      this.postImages.push('data:image/gif;base64,' + imgs[i].base64);
    }
    if (this.postImages !== null) {
      setTimeout(() => { this.isImgShow = true; }, 200);
    }
  }

  addImage(event: any) {
    const _self = this;
    const reader = new FileReader();
    if (event.target.files && event.target.files.length > 0) {
      const file = event.target.files[0];
      reader.readAsDataURL(file);
      reader.onloadend = () => {
        const res = reader.result as string;
        $('.add_img').css('background-image',
          'url(data:image/gif;base64,' + res.split(',')[1] + ')');
        $('.add_img').css('background-size',
          'contain');

        const img: PostImage = {
          id: 0,
          filename: file.name,
          filetype: file.type,
          base64: res.split(',')[1],
          isShow: true,
          url: ''
        };
        console.log(img);
        _self.post.images.push(img);
        _self.isImgShow = false;
        _self.setImages(this.post.images);
      };

    }
  }
  save() {
    this.post.creator=new User();
    this.postService.addPost(this.post).subscribe(res => {
      console.log(res);
      this.router.navigate(['']);
    });
  }
}
