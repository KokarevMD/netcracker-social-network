import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { LoginService } from 'src/app/services/login.service';
import { MatDialog } from '@angular/material/dialog';
import { User } from 'src/app/entities/User';
import * as $ from 'jquery';
import { ProfileImage } from '../entities/ProfileImage';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  user?: User;
  buffUser?: User = new User();
  element: any = null;
  elementEdit: any = null;
  isOpenBlock: string = "";
  isCheckPassword = false;
  newPsaaword:string = "";
  newPsaawordconfirm:string = "";

  constructor(private userService: UserService,
    private loginService: LoginService,
    private dialogRef: MatDialog,
    private router: Router,
    private snackBar:MatSnackBar) { }

  ngOnInit(): void {
    this.userService.getLoginUser().subscribe(res => {
      this.user = res;
      this.buffUser =res;
      if (this.user?.profileImage?.base64 !== undefined) {
        $('.add_img').css('background-image',
          'url(data:image/gif;base64,' + this.user.profileImage.base64 + ')');
        $('.add_img').css('background-size',
          'contain');
      }
    });
  }
  
  onFileChange(event:any) {
    const _self = this;
    const reader = new FileReader();
    if (event.target.files && event.target.files.length > 0) {
      const file = event.target.files[0];
      reader.readAsDataURL(file);
      reader.onloadend = () => {
        const res = reader.result as string;
        $('.add_img').css('background-image',
          'url(data:image/gif;base64,' + res.split(',')[1] + ')');
        $('.add_img').css('background-size',
          'contain');

        const img: ProfileImage = {
          id: 0,
          filename: file.name,
          filetype: file.type,
          base64: res.split(',')[1],
          isShow: true,
          url: ''
        };
        _self.user!.profileImage! = img;
      };
    }
  }
  checkToEnter() {
    $(document).ready(() => {
      $('input').keydown(e => {
        if (e.keyCode === 13) {
          this.closeEdit(true, this.isOpenBlock);
        }
      });
    });
  }

  closeEdit(correct: boolean, id: string): void {
    this.buffUser!.password = '';
    // this.oldPsaaword = '';
    this.isCheckPassword = false;
    this.isOpenBlock = "";
    this.element = $(`#block-${id}`);
    this.elementEdit = $(`#isHide-block-${id}`);
    this.element.toggleClass('isHidenBlock');
    this.elementEdit.toggleClass('isHidenBlock');
    if (correct&&id!='password') {
     this.user=this.buffUser;
    } if(correct&&id=='password'){
      this.userService.editPass(this.user!,this.newPsaaword).subscribe(res=>{
        console.log(res);
      });
    }
  }


  openForEdit(idElement: string): void {
    if (this.isOpenBlock != null) {
      this.element = $(`#block-${this.isOpenBlock}`);
      this.elementEdit = $(`#isHide-block-${this.isOpenBlock}`);
      this.element.toggleClass('isHidenBlock');
      this.elementEdit.toggleClass('isHidenBlock');
    }
    this.buffUser = { ...this.user! };
    this.isOpenBlock = idElement;
    this.element = $(`#block-${idElement}`);
    this.elementEdit = $(`#isHide-block-${idElement}`);
    this.element.toggleClass('isHidenBlock');
    this.elementEdit.toggleClass('isHidenBlock');
  }

  checkToPassword(): void {
    this.userService.checkPass(this.buffUser!.password).subscribe(res=>{
      if(res==false){
        this.snackBar.open('Password is wrong', 'Ok :(', {
          duration: 3000
        });
      }
      this.isCheckPassword=res;
    })
   
  }

  saveChanges(){
    this.userService.editUser(this.user!).subscribe(res=>{
      this.router.navigateByUrl(`/`);
    });
  }
}