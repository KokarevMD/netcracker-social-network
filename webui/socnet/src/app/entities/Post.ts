import { PostImage } from "./PostImage";
import { User } from "./User";

export class Post{
    id!:number;
    creator!:User;
    date!:Date;
    postText!:String;
    images!:PostImage[];
    likes!:number;
    dislikes!:number;

    constructor(
        id:number=0,
        creator:User=new User(),
        date:Date = new Date(),
        postText:String='',
        images:PostImage[]=new Array(),
        likes:number=0,
        dislikes:number=0){
            this.id=id,
            this.creator=creator;
            this.date=date;
            this.postText=postText;
            this.images=images;
            this.likes=likes;
            this.dislikes=dislikes;
        }

        public static getInstance(v:any) {
            if (v == null) {
                return new Post(0);
            }
            const post = new Post(
                v.id,
                v.creator,
                v.date,
                v.postText,
                v.images,
                v.likes,
                v.dislikes
            );
            console.log(v);
            return post;
        }
}