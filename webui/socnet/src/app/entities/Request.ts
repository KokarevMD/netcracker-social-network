import { User } from "./User";

export class Request {
    id?:number=0;
    source!: User;
    target!: User;

    constructor(id:number,source: User, target: User) {
        this.id=id;
        this.target = target;
        this.source = source;
    }
}