import { User } from "./User";

export class Comment{
   id!: number;
   postId!: number;
   message!: string;
   user!: User;
   dateAndTime!: Date;

  constructor(id: number=0,
              postId: number=0,
              message: string='',
              user: User=new User(),
              dateAndTime: Date=new Date()) {
      this.id = id;
      this.postId = postId;
      this.message = message;
      this.user = user;
      this.dateAndTime = dateAndTime;
  }
  public static getInstance(v:any) {
    if (v == null) {
        return new Comment(0);
    }
    const post = new Comment(
        v.id,
        v.postId,
        v.message,
        v.user,
        v.dateAndTime
    );
    console.log(v);
    return post;
}
}
