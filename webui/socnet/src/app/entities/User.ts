import { ProfileImage } from './ProfileImage';

export class User {
  userId:string;
  password:string;
  company:string;
  name:string;
  surname:string;
  email:string;
  profileImage?: ProfileImage;
  phone:string;
  country:string;
  role:string;
  gender:string;

  constructor( userId:string="new",
    password:string="",
    company:string="",
    name:string="",
    surname:string="",
    email:string="",
    phone:string="",
    country:string="",
    role:string="USER",
    gender:string="MALE") {
      this.userId = userId
      this.password = password;
      this.company = company;
      this.name = name;
      this.surname = surname;
      this.email = email;
      this.phone = phone;
      this.country = country;
      this.role = role;
      this.gender = gender;
  }
}
