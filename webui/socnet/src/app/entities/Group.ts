import { User } from "./User";

export class Group{
    id:number;
    creator:User;
    name:string;
    users:User[];

    constructor(id:number=0,
        creator:User=new User(),
        name:string='',
        users=new Array()){
            this.id=id;
            this.creator=creator;
            this.name=name;
            this.users=users;
        }

        public static getInstance(v:any) {
            if (v == null) {
                return new Group(0);
            }
            const group = new Group(
                v.id,
                v.creator,
                v.name,
                v.users,
            );
            console.log(v);
            return group;
        }
}