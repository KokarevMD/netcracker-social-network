export class RegisterUser{
  userId!: string;
  password!: string;
  confirmPassword!: string;
  company!: string;
  name!: string;
  surname!: string;
  email!: string;
  phone!: string;
  country!: string;
  role!: string;
  gender!: string;
}