import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { Group } from "../entities/Group";
import { ApiRequestService } from "./api-request.service";



@Injectable()
export class GroupService {
    constructor(private apiRequest: ApiRequestService) {
    }

    getGroupsForUser(userId:string): Observable<any> {
        return this.apiRequest.get(`api/groups/all/${userId}`)
            .pipe(map((jsonResp) => {
                return jsonResp.map(Group.getInstance);
            }));
    }

    getGroupsForJoin(userId:string): Observable<any> {
        return this.apiRequest.get(`api/groups/all/join/${userId}`)
            .pipe(map((jsonResp) => {
                return jsonResp.map(Group.getInstance);
            }));
    }

    addGroup(group:Group): Observable<any>{
        return this.apiRequest.post(`api/groups/add`,group);
    }
    deleteGroup(groupId:number): Observable<any>{
        return this.apiRequest.delete(`api/groups/del/${groupId}`);
    }

    joinGroup(groupId:number){
        return this.apiRequest.post(`api/groups/join/${groupId}`,'');
    }

    leaveGroup(groupId:number){
        return this.apiRequest.post(`api/groups/leave/${groupId}`,'');
    }
}