import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { Post } from "../entities/Post";
import { ApiRequestService } from "./api-request.service";
import { Comment } from 'src/app/entities/Comment';


@Injectable()
export class PostService {
    constructor(private apiRequest: ApiRequestService) {
    }
    getFriendsPosts(userId:string): Observable<any> {
        return this.apiRequest.get(`api/post/all/${userId}`)
            .pipe(map((jsonResp) => {
                return jsonResp.map(Post.getInstance);
            }));
    }

    addPost(post:Post): Observable<any>{
        return this.apiRequest.post(`api/post/add`,post);
    }

    addLike(postId:number){
        return this.apiRequest.get(`api/post/like/add/${postId}`);
    }
    removeLike(postId:number){
        return this.apiRequest.get(`api/post/like/remove/${postId}`);
    }
    addDislike(postId:number){
        return this.apiRequest.get(`api/post/dislike/add/${postId}`);
    }
    removeDislike(postId:number){
        return this.apiRequest.get(`api/post/dislike/remove/${postId}`);
    }
    addComment(comment:Comment){
        return this.apiRequest.post(`api/comment/add`,comment);
    }
    getComments(postId:number){
        return this.apiRequest.get(`api/comment/all/${postId}`)
        .pipe(map((jsonResp) => {
            return jsonResp.map(Comment.getInstance);
        }));;
    }
}