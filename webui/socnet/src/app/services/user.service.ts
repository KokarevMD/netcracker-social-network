import { Injectable } from '@angular/core';
import { ApiRequestService } from './api-request.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { User } from '../entities/User';
import { Request } from '../entities/Request';


@Injectable()
export class UserService {
    constructor(private apiRequest: ApiRequestService) {

    }

    getLoginUser(): Observable<any> {
        let user;
        return this.apiRequest.get('user/get')
            .pipe(map(jsonResp => {
                user = new User(
                    jsonResp.userId,
                    jsonResp.password,
                    jsonResp.company,
                    jsonResp.name,
                    jsonResp.surname,
                    jsonResp.email,
                    jsonResp.phone,
                    jsonResp.country,
                    jsonResp.role,
                    jsonResp.gender,
                );
                user.profileImage = jsonResp.profileImage;
                if (user.profileImage == null) {
                    user.profileImage = {
                        filename: '',
                        filetype: '',
                        isShow: true,
                        url: 'assets/images/courses/img.jpg'
                    };
                }
                return user;
            }
            ));
    }

    editUser(user: User): Observable<any> {
        return this.apiRequest.post('user/edit', user);
    }

    checkPass(pass:string){
        return this.apiRequest.post(`user/check`,pass);
    }

    editPass(user:User,pass:String){
        return this.apiRequest.post(`user/edit/pass/${user.userId}`,pass);
    }

    getUsers(): Observable<any> {
        return this.apiRequest.get('user/all')
            .pipe(map(jsonResp => {
                return jsonResp.map(this.mapUser);
            }));
    }
    getFriends(user:User): Observable<any> {
        return this.apiRequest.get(`friends/${user.userId}`)
            .pipe(map(jsonResp => {
                return jsonResp.map(this.mapUser);
            }));
    }

    getFriendCandidates(user:User): Observable<any> {
        return this.apiRequest.get(`friends/all/${user.userId}`)
            .pipe(map(jsonResp => {
                return jsonResp.map(this.mapUser);
            }));
    }

    addRequest(request: Request): Observable<any> {
        return this.apiRequest.post('friends/addRequest', request);
    }

    handleRequest(isAccepted:boolean,request: Request): Observable<any> {
        return this.apiRequest.post(`friends/handle/${isAccepted}`, request);
    }

    deleteFriend(userId:string,friend:User): Observable<any> {
        return this.apiRequest.post(`friends/del/${userId}`, friend);
    }
    
    getRequests(user:User): Observable<any> {
        return this.apiRequest.get(`friends/req/all/${user.userId}`)
            .pipe(map(jsonResp => {
                return jsonResp.map(this.mapRequest);
            }));
    }


    private mapRequest(v:any, _i?:any, _a?:any): Request{
        const req = new Request(
            v.id,
            v.source,
            v.target
        );
        return req;
    }
    private mapUser(v: any, _i?: any, _a?: any): User {
        const user = new User(
            v.userId,
            v.password,
            v.company,
            v.name,
            v.surname,
            v.email,
            v.phone,
            v.country,
            v.role,
            v.gender,
        );
        user.profileImage = v.profileImage;
        if (user.profileImage == null) {
            user.profileImage = {
                filename: '',
                filetype: '',
                isShow: true,
                url: 'assets/images/courses/img.jpg'
            };
        }
        return user;
    }


    getUserInfo(userId: string) {
        let user;
        return this.apiRequest.get(`user/info/${userId}`).pipe(map(jsonResp => {
            user = new User(
                jsonResp.userId,
                jsonResp.password,
                jsonResp.company,
                jsonResp.name,
                jsonResp.surname,
                jsonResp.email,
                jsonResp.phone,
                jsonResp.country,
                jsonResp.role,
                jsonResp.gender,
            );
            user.profileImage = jsonResp.profileImage;
            if (user.profileImage == null) {
                user.profileImage = {
                    filename: '',
                    filetype: '',
                    isShow: true,
                    url: 'assets/images/courses/img.jpg'
                };
            }
            return user;
        }
        ));
    }
}
