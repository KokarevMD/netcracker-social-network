import { Component, OnInit, ViewChild } from '@angular/core';
import { User } from '../entities/User';
import * as $ from 'jquery';
import { UserService } from '../services/user.service';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { AddRequestComponent } from './add-request/add-request.component';

@Component({
  selector: 'app-friends',
  templateUrl: './friends.component.html',
  styleUrls: ['./friends.component.css']
})
export class FriendsComponent implements OnInit {
  user:User=new User();
  users:User[]=new Array();
  friends:User[]=new Array();
  displayedColumns: string[] = ['userId', 'name', 'surname','accept'];
  dataSource!: MatTableDataSource<any>;

  constructor(private userService:UserService,private dialogRef:MatDialog) { }

  @ViewChild(MatSort, {static: true}) sort!: MatSort;
  @ViewChild(MatPaginator, {static: true}) paginator!: MatPaginator;

  ngOnInit(): void {
    this.userService.getLoginUser().subscribe(res=>{
      this.user=res;
      this.getFriends(res);
      this.getCandidates(res);
    });
  }

  tableInit(users:User[]): void {
    this.dataSource = new MatTableDataSource<User>(users);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(event: Event): void {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  getFriends(user:User){
    this.userService.getFriends(user).subscribe(res=>{
      this.friends=res;
      console.log(res);
    })
  }
  getCandidates(user:User){
    this.userService.getFriendCandidates(user).subscribe(res=>{
      this.users=res;
      this.tableInit(res);
      console.log(res);
    })
  }
  openAddRequest(user: User): void {
    const dialog = this.dialogRef.open(AddRequestComponent, {
      width: '250px',
      data: {
        user
      }
    });

    dialog.afterClosed().subscribe(res=>{
      if(res!==undefined){
        let i = this.users.indexOf(user);
        this.users.splice(i,1);
        this.tableInit(this.users);
      }
    });
  }
}
