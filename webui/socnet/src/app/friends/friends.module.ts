import { NgModule } from '@angular/core';
import {MatRadioModule} from '@angular/material/radio';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { FormsModule } from '@angular/forms';
import { MatDialogModule } from '@angular/material/dialog';
import { CommonModule } from '@angular/common';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { FriendsRoutingModule } from './friends-routing.module';
import { MatTableModule } from '@angular/material/table';
import { FriendsComponent } from './friends.component';
import { AddRequestComponent } from './add-request/add-request.component';
import { RequestsComponent } from './requests/requests.component';
import { HandleRequestsComponent } from './requests/handle-requests/handle-requests.component';
import { MyFriendsComponent } from './my-friends/my-friends.component';
import { DeleteFriendComponent } from './my-friends/delete-friend/delete-friend.component';



@NgModule({
  declarations: [
    FriendsComponent,
    AddRequestComponent,
    RequestsComponent,
    HandleRequestsComponent,
    MyFriendsComponent,
    DeleteFriendComponent
  ],
  imports: [
    CommonModule,
    FriendsRoutingModule,
    MatDialogModule,
    MatIconModule,
    MatInputModule,
    MatRadioModule,
    MatFormFieldModule,
    MatTableModule,
    MatFormFieldModule,
    FormsModule,
    MatRadioModule,
    MatSnackBarModule,
    MatProgressSpinnerModule
  ]
})
export class FriendsModule { }
