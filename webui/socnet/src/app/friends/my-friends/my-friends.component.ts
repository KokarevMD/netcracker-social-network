import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { User } from 'src/app/entities/User';
import { UserService } from 'src/app/services/user.service';
import { AddRequestComponent } from '../add-request/add-request.component';
import { DeleteFriendComponent } from './delete-friend/delete-friend.component';

@Component({
  selector: 'app-my-friends',
  templateUrl: './my-friends.component.html',
  styleUrls: ['./my-friends.component.css']
})
export class MyFriendsComponent implements OnInit {
  user:User=new User();
  friends:User[]=new Array();
  displayedColumns: string[] = ['userId', 'name', 'surname','accept'];
  dataSource!: MatTableDataSource<any>;

  constructor(private userService:UserService,private dialogRef:MatDialog) { }

  @ViewChild(MatSort, {static: true}) sort!: MatSort;
  @ViewChild(MatPaginator, {static: true}) paginator!: MatPaginator;

  ngOnInit(): void {
    this.userService.getLoginUser().subscribe(res=>{
      this.user=res;
      this.getFriends(res);
    });
  }

  tableInit(friends:User[]): void {
    this.dataSource = new MatTableDataSource<User>(friends);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(event: Event): void {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  getFriends(user:User){
    this.userService.getFriends(user).subscribe(res=>{
      this.friends=res;
      console.log(res);
      this.tableInit(this.friends);
    })
  }
  
  openDeleteFriend(user: User): void {
    const dialog = this.dialogRef.open(DeleteFriendComponent, {
      width: '250px',
      data: {
        user
      }
    });

    dialog.afterClosed().subscribe(res=>{
      if(res!=undefined){
        let i = this.friends.indexOf(res);
        this.friends.splice(i,1);
        this.tableInit(this.friends);
      }
    });
  }

}
