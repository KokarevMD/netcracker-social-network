import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { User } from 'src/app/entities/User';
import { UserService } from 'src/app/services/user.service';
import { AddRequestComponent } from '../../add-request/add-request.component';

@Component({
  selector: 'app-delete-friend',
  templateUrl: './delete-friend.component.html',
  styleUrls: ['./delete-friend.component.css']
})
export class DeleteFriendComponent implements OnInit {

  thisUser!:User;
  user!:User;
  constructor(public dialogRef: MatDialogRef<AddRequestComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,private userService:UserService) { }

  ngOnInit(): void {
    this.userService.getLoginUser().subscribe(res=>{
      this.thisUser=res;
    });
    this.user = this.data.user;
  }
  onYesClick() {
    this.userService.deleteFriend(this.thisUser.userId,this.user).subscribe(res=>{});
    this.dialogRef.close(this.user);
  }

  onNoClick() {
    this.dialogRef.close();
  }
}
