import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Request } from 'src/app/entities/Request';
import { User } from 'src/app/entities/User';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-add-request',
  templateUrl: './add-request.component.html',
  styleUrls: ['./add-request.component.css']
})
export class AddRequestComponent implements OnInit {
  thisUser!:User;
  user!:User;
  constructor(public dialogRef: MatDialogRef<AddRequestComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,private userService:UserService) { }

  ngOnInit(): void {
    this.userService.getLoginUser().subscribe(res=>{
      this.thisUser=res;
    });
    this.user = this.data.user;
  }
  onYesClick() {
    let request = new Request(0,this.thisUser,this.user);
    this.userService.addRequest(request).subscribe(res=>{
        console.log(res);
    });
    this.dialogRef.close(this.user);
  }

  onNoClick() {
    this.dialogRef.close();
  }
}
