import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FriendsComponent } from './friends.component';
import { MyFriendsComponent } from './my-friends/my-friends.component';
import { RequestsComponent } from './requests/requests.component';


const routes: Routes =[
{ path: '', component: FriendsComponent },
{ path: 'requests', component: RequestsComponent },
{ path: 'my', component: MyFriendsComponent },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FriendsRoutingModule { }
