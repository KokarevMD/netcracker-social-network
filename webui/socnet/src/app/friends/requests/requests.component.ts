import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Request } from 'src/app/entities/Request';
import { User } from 'src/app/entities/User';
import { UserService } from 'src/app/services/user.service';
import { AddRequestComponent } from '../add-request/add-request.component';
import { HandleRequestsComponent } from './handle-requests/handle-requests.component';

@Component({
  selector: 'app-requests',
  templateUrl: './requests.component.html',
  styleUrls: ['./requests.component.css']
})
export class RequestsComponent implements OnInit {
  user:User=new User();
  requests:Request[]= new Array();
  displayedColumns: string[] = ['userId', 'name', 'surname','accept'];
  dataSource!: MatTableDataSource<any>;

  constructor(private userService:UserService,private dialogRef:MatDialog) { }

  @ViewChild(MatSort, {static: true}) sort!: MatSort;
  @ViewChild(MatPaginator, {static: true}) paginator!: MatPaginator;

  ngOnInit(): void {
    this.userService.getLoginUser().subscribe(res=>{
      this.user=res;
      this.getRequests(res);
    });
  }

  tableInit(reqs:Request[]): void {
    this.dataSource = new MatTableDataSource<Request>(reqs);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(event: Event): void {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  getRequests(user:User){

    this.userService.getRequests(user).subscribe(res=>{
      this.requests=res;
      this.tableInit(res);
    });
  }
  openHandleRequests(req: Request): void {
    const dialog = this.dialogRef.open(HandleRequestsComponent, {
      width: '250px',
      data: {
        req
      }
    });

    dialog.afterClosed().subscribe(res=>{
      let i = this.requests.indexOf(res);
      this.requests.splice(i,1);
      this.tableInit(this.requests);
    });
  }
}

