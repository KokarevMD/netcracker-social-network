import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { User } from 'src/app/entities/User';
import { Request } from 'src/app/entities/Request';
import { UserService } from 'src/app/services/user.service';
import { AddRequestComponent } from '../../add-request/add-request.component';

@Component({
  selector: 'app-handle-requests',
  templateUrl: './handle-requests.component.html',
  styleUrls: ['./handle-requests.component.css']
})
export class HandleRequestsComponent implements OnInit {
  thisUser!:User;
  req!:Request;
  constructor(public dialogRef: MatDialogRef<AddRequestComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,private userService:UserService) { }

  ngOnInit(): void {
    this.userService.getLoginUser().subscribe(res=>{
      this.thisUser=res;
    });
    this.req = this.data.req;
  }
  onYesClick() {
    this.userService.handleRequest(true,this.req).subscribe(res=>{});
    this.dialogRef.close(this.req);
  }

  onNoClick() {
    this.userService.handleRequest(false,this.req).subscribe(res=>{});
    this.dialogRef.close(this.req);
  }
}
