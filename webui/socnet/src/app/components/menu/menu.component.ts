import { Component, OnInit, AfterContentInit, AfterContentChecked, AfterViewInit, ElementRef, ViewChild } from '@angular/core';
import * as $ from 'jquery';
import { UserService } from 'src/app/services/user.service';
import { LoginService } from 'src/app/services/login.service';
import { Router, NavigationEnd } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { UserInfoService } from 'src/app/services/user-info.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {
  role!: string;
  hide = false;

  constructor(private loginService: LoginService, private userService: UserService, private router: Router) {
  }


  ngOnInit(): void {
    if (this.router.url === '/login' || this.router.url === '/login/reg') {
      this.hide = true;
    } else {
      this.hide = false;
    }
    this.router.events.subscribe(changes => {
      if (changes instanceof NavigationEnd) {
        if (changes.urlAfterRedirects === '/login' || changes.urlAfterRedirects === '/login/reg') {
          this.hide = true;
        } else {
          this.hide = false;
        }
      }
    });
    this.updateMenu();
    this.loginService.isLoginEvent$.subscribe((data) => {
      this.updateMenu();
    });
  }

  f() {
    this.userService.getLoginUser().subscribe((res: any)=>{
      console.log(res);
    });
  }

  selectNavigation(event: any): void {
    $('#top-menu').find('.active').removeClass('active');
    const el = $(event.target);
    el.addClass('active');
  }

  updateMenu() {
    // this.userService.getLoginUser().subscribe(user => {
    //   this.role = user.role;
    // });
    // this.role = this.userInfoService.getUserRole();
    this.f();
  }
}
