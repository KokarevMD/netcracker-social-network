import { Component, OnInit } from '@angular/core';
import { UserInfoService } from 'src/app/services/user-info.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.scss']
})
export class LogoutComponent {

  constructor(private userInfoService: UserInfoService, private router: Router) {
    this.userInfoService.removeUserInfo();
    this.router.navigate(['/login']);
  }
}
