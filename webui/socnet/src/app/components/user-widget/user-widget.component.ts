import { Component, OnInit, Input } from '@angular/core';
import { UserInfoService, UserInStorage } from 'src/app/services/user-info.service';
import { LoginService } from 'src/app/services/login.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-user-widget',
  templateUrl: './user-widget.component.html',
  styleUrls: ['./user-widget.component.scss']
})
export class UserWidgetComponent implements OnInit {
  isLoggedIn = false;
  userName!: string;
  role!:string;
  constructor(private userInfoService: UserInfoService, private loginService: LoginService) { }

  ngOnInit(): void {
    this.checkLogin();
    this.loginService.isLoginEvent$.subscribe((data) => {
      this.checkLogin();
      // this.role=this.userInfoService.getUserRole();
    });
  }

  checkLogin(): void {
    this.isLoggedIn = this.userInfoService.isLoggedIn();
    if (this.isLoggedIn) {
      this.userName = this.userInfoService.getUserName();
    }
  }
  logOut(){
    this.loginService.logout();
  }
}
