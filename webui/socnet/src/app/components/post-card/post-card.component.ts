import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { NgxSwiperConfig } from 'ngx-image-swiper';
import { Post } from 'src/app/entities/Post';
import * as $ from 'jquery';
import { PostImage } from 'src/app/entities/PostImage';
import { PostService } from 'src/app/services/post.service';
import { User } from 'src/app/entities/User';
import { Comment } from 'src/app/entities/Comment';
import { UserService } from 'src/app/services/user.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-post-card',
  templateUrl: './post-card.component.html',
  styleUrls: ['./post-card.component.css']
})
export class PostCardComponent implements OnInit {
  @Input() post!: Post;
  @Output() outputItem = new EventEmitter();
  postImages: [string] = [''];
  isImgShow: boolean = false;
  swiperConfig: NgxSwiperConfig = {
    navigation: true,
    navigationPlacement: 'inside',
    pagination: false,
    paginationPlacement: 'inside',
    loop: true,
    imgBackgroundSize: 'contain',

  };


  //-comments-\\

  allsId: number[] = new Array();
  comments: Comment[] = new Array();
  textNewMessage!: string;
  user: User = new User();
  answeredMessage!: Comment;

  greeting: any;
  name!: string;

  editMeMessage!: Comment;
  isAnswer: boolean = false;
  isLoad: boolean = false;
  isEdit: boolean = false;
  isMe: boolean = false;
  timerId: any;
  isCtrl: boolean = false;
  discussionShowAllAnswer: boolean[] = new Array();
  textEditMessage!: string;

  stompClient: any;



  //---------\\
  constructor(private postService: PostService,private userService:UserService, private snackBar:MatSnackBar) { }

  initKeyToSend(): void {
    $(document).ready(() => {
      $('textarea').keyup(e => {
        const text = e.keyCode +
          (e.ctrlKey ? '17' : '') + '\n';
        const my = text;
        if (+my == 1317) {
          this.addComment();
        }
      });
    });
  }

  ngOnInit(): void {
    this.setImages(this.post.images);
    this.setComments();
    this.userService.getLoginUser().subscribe(user=>{this.user=user});
  }

  setComments(){
    this.postService.getComments(this.post.id).subscribe(res=>{this.comments=res});
  }


  setImages(imgs: PostImage[]) {
    this.postImages.splice(0, this.postImages.length);

    for (let i = 0; i < imgs.length; i++) {
      this.postImages.push('data:image/gif;base64,' + imgs[i].base64);
    }
    if (this.postImages !== null) {
      setTimeout(() => { this.isImgShow = true; }, 200);
    }
  }
  like(event: any) {
    if ($('.like.' + this.post.id).hasClass('icon')) {
      if($('.dislike.' + this.post.id).hasClass('icon_active')){
        $('.dislike.' + this.post.id).toggleClass('icon_active icon');
        this.post.dislikes -= 1;
        this.postService.removeDislike(this.post.id).subscribe(res => { });
      }
      $('.like.' + this.post.id).toggleClass('icon icon_active');
      this.post.likes += 1;
      this.postService.addLike(this.post.id).subscribe(res => { });
    } else {
      $('.like.' + this.post.id).toggleClass('icon_active icon');
      this.post.likes -= 1;
      this.postService.removeLike(this.post.id).subscribe(res => { });
    }
  }

  dislike(event: any) {
    if ($('.dislike.' + this.post.id).hasClass('icon')) {
      if($('.like.' + this.post.id).hasClass('icon_active')){
        $('.like.' + this.post.id).toggleClass('icon_active icon');
        this.post.likes -= 1;
        this.postService.removeDislike(this.post.id).subscribe(res => { });
      }
      $('.dislike.' + this.post.id).toggleClass('icon icon_active');
      this.post.dislikes += 1;
      this.postService.addDislike(this.post.id).subscribe(res => { });
    } else {
      $('.dislike.' + this.post.id).toggleClass('icon_active icon');
      this.post.dislikes -= 1;
      this.postService.removeDislike(this.post.id).subscribe(res => { });

    }


  }

   //-Comments
  addComment() {
    if(this.textNewMessage.length<=10000){
      const newComment = new Comment(0, this.post.id, this.textNewMessage, this.user, new Date());
      this.isMe = true;
      this.postService.addComment(newComment).subscribe(res => { });
      this.comments.push(newComment);
      this.textNewMessage = "";
    } else{
      this.snackBar.open('Comment too long :(', 'Ok :(', {
        duration: 3000
      });
    }
    
  }

  cancelAnswerMessage(): void {
    $('#isAnswerMessage').toggleClass('block_messeger_open');
    this.isAnswer = false;
    this.initKeyToSend();
  }
}