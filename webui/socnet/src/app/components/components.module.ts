import { NgModule } from '@angular/core';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatBadgeModule} from '@angular/material/badge';
import { MatMenuModule } from '@angular/material/menu';
import { CommonModule } from '@angular/common';
import { MatIconModule } from '@angular/material/icon';
import { PostCardComponent } from './post-card/post-card.component';



@NgModule({
  declarations: [
  ],
  exports: [
  ],
  imports: [
    MatTooltipModule,
    MatBadgeModule,
    MatMenuModule,
    CommonModule,
    MatIconModule,
  ]
})
export class ComponentsModule { }
