package com.SocNet.proj.dto;


import com.SocNet.proj.model.user.Gender;
import com.SocNet.proj.model.user.ProfileImage;
import com.SocNet.proj.model.user.Role;
import com.SocNet.proj.model.user.User;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.beans.BeanUtils;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Getter
@Setter
@NoArgsConstructor
public class UserDto {
    private String userId;
    private String password = "";
    private String company;
    private String name;
    private String surname;
    private String email;
    private ProfileImageDto profileImage;
    private String phone;
    private String country;
    private Role role;
    private Gender gender;

    public static UserDto convertFromEntity(User user) {
        UserDto dto = new UserDto();
        BeanUtils.copyProperties(user, dto, "password","profileImage");
        dto.setProfileImage(ProfileImageDto.convertFromEntity(user.getProfileImage()));
        return dto;
    }

    public static List<UserDto> convertFromEntities(List<User> users) {
        return users.stream().map(UserDto::convertFromEntity).collect(Collectors.toList());
    }
}
