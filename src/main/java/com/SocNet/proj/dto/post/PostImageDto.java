package com.SocNet.proj.dto.post;

import com.SocNet.proj.model.post.PostImage;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.beans.BeanUtils;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
@Getter
@Setter
@NoArgsConstructor
public class PostImageDto {
    private String url;
    private int sort;
    private boolean isShow = true;
    private String filename;
    private String filetype;
    private String base64;

    public static PostImageDto convertFromEntity(PostImage postImage) {
        if (Objects.isNull(postImage)) {
            return null;
        }
        PostImageDto dto = new PostImageDto();
        BeanUtils.copyProperties(postImage, dto);
        return dto;
    }

    public static List<PostImageDto> convertFromEntities(List<PostImage> itemImages) {
        return Objects.isNull(itemImages) ? null : itemImages.stream().map(PostImageDto::convertFromEntity).collect(Collectors.toList());
    }

    public static PostImage convertToEntity(PostImageDto dto) {
        if (Objects.isNull(dto)) {
            return null;
        }
        PostImage postImage = new PostImage();
        BeanUtils.copyProperties(dto, postImage);
        return postImage;
    }

    public static List<PostImage> convertToEntities(List<PostImageDto> dtos) {
        return Objects.isNull(dtos) ? null : dtos.stream().map(PostImageDto::convertToEntity).collect(Collectors.toList());
    }
}
