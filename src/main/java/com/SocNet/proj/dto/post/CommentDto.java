package com.SocNet.proj.dto.post;

import com.SocNet.proj.dto.UserDto;
import com.SocNet.proj.model.post.Comment;
import com.SocNet.proj.model.post.Post;
import com.SocNet.proj.model.user.User;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.beans.BeanUtils;

import java.util.Date;
import java.util.List;
import java.util.Objects;

@Getter
@Setter
@NoArgsConstructor
public class CommentDto {
    private Long id;
    private Long postId;
    private String message;
    private UserDto user;
    private Date dateAndTime;

    public static CommentDto convertFromEntity(Comment comment, UserDto userDto, Long postId) {
        if (Objects.isNull(comment) || Objects.isNull(userDto) ||  Objects.isNull(postId)) {
            return null;
        }
        CommentDto commentDto = new CommentDto();
        BeanUtils.copyProperties(comment, commentDto, "user", "courseId");
        commentDto.setUser(userDto);
        commentDto.setPostId(postId);
        return commentDto;
    }

    public static Comment convertToEntity(CommentDto commentDto, Post post, User user) {
        if (Objects.isNull(commentDto) || Objects.isNull(post) || Objects.isNull(user)) {
            return null;
        }
        Comment comment = new Comment();
        BeanUtils.copyProperties(commentDto, comment, "user", "postId");
        comment.setUser(user);
        comment.setPost(post);
        return comment;
    }
}
