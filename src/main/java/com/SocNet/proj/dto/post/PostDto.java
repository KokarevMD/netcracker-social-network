package com.SocNet.proj.dto.post;

import com.SocNet.proj.dto.UserDto;
import com.SocNet.proj.model.post.Post;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.beans.BeanUtils;

import java.time.LocalDate;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class PostDto {
    Long id;
    UserDto creator;
    LocalDate date;
    String postText;
    private List<PostImageDto> images;
    int likes;
    int dislikes;

    public static PostDto convertFromEntity(Post post) {
        PostDto dto = new PostDto();
        BeanUtils.copyProperties(post, dto,"creator","images");
        dto.setCreator(UserDto.convertFromEntity(post.getCreator()));
        dto.setImages(PostImageDto.convertFromEntities(post.getImages()));
        return dto;
    }
}
