package com.SocNet.proj.dto;


import com.SocNet.proj.model.user.ProfileImage;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.beans.BeanUtils;

import javax.persistence.ManyToOne;
import java.util.Objects;

@Getter
@Setter
@NoArgsConstructor
public class ProfileImageDto {

    private String url;
    private int sort;
    private boolean isShow = true;
    private String filename;
    private String filetype;
    private String base64;

    public static ProfileImageDto convertFromEntity(ProfileImage profileImage) {
        if (Objects.isNull(profileImage)) {
            return null;
        }
        ProfileImageDto dto = new ProfileImageDto();
        BeanUtils.copyProperties(profileImage, dto);
        return dto;
    }
}
