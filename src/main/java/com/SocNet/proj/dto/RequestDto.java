package com.SocNet.proj.dto;

import com.SocNet.proj.model.user.request.Request;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@NoArgsConstructor
public class RequestDto {
    private Long id;
    private UserDto source;
    private UserDto target;

    public static RequestDto convertFromEntity(Request request) {
        RequestDto dto = new RequestDto();
        dto.id = request.getId();
        dto.source = UserDto.convertFromEntity(request.getSource());
        dto.target = UserDto.convertFromEntity(request.getTarget());
        return dto;
    }
    public static List<RequestDto> convertFromEntities(List<Request> requests) {
        return requests.stream().map(RequestDto::convertFromEntity).collect(Collectors.toList());
    }
}
