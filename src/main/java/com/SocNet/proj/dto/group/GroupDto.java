package com.SocNet.proj.dto.group;


import com.SocNet.proj.dto.UserDto;
import com.SocNet.proj.model.group.Group;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.beans.BeanUtils;

import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@NoArgsConstructor
public class GroupDto {
    private Long id;
    private String name;
    private UserDto creator;
    private List<UserDto> users;

    public static GroupDto convertFromEntity(Group group){
        GroupDto dto=new GroupDto();
        BeanUtils.copyProperties(group,dto,"users","creator");
        dto.setUsers(UserDto.convertFromEntities(group.getUsers()));
        dto.setCreator(UserDto.convertFromEntity(group.getCreator()));
        return dto;
    }

    public static List<GroupDto> convertFromEntities(List<Group> groups){
        return groups.stream().map(GroupDto::convertFromEntity).collect(Collectors.toList());
    }
}
