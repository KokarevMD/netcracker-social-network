package com.SocNet.proj.model.user;

import com.SocNet.proj.model.BaseEntity;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Type;

import javax.persistence.*;


@Setter
@Getter
@NoArgsConstructor
@Entity
@Table(name = "profile_images")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class ProfileImage extends BaseEntity {
    private String url;

    private int sort;

    private boolean isShow = true;

    private String filename;

    private String filetype;

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    private String base64;

    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    User user;
}
