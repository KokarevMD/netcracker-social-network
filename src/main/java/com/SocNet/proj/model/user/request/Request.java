package com.SocNet.proj.model.user.request;

import com.SocNet.proj.model.BaseEntity;
import com.SocNet.proj.model.user.User;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity(name = "m_request")
@Getter
@Setter
public class Request extends BaseEntity {

    @ManyToOne()
    User source;
    @ManyToOne()
    User target;
}
