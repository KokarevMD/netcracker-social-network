package com.SocNet.proj.model.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@Entity(name = "m_User")
public class User {

    @Id
    private String userId;
    private String password = "";
    private String company;
    private String name;
    private String surname;
    private String email;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "profile_image_id")
    private ProfileImage profileImage;
    private String phone;
    private String country;

    @Enumerated(EnumType.STRING)
    private Role role;

    @Enumerated(EnumType.STRING)
    private Gender gender;

    @OneToMany(cascade = CascadeType.ALL)
    List<User> friends;

    public void addFriend(User friend){
        friends.add(friend);
    }
    public List<User> delFriend(User friend){
        friends.remove(friend);
        return friends;
    }
    public User(String userId, String password, Role role, String name, String surname, String company, String country, String phone) {
        this.setUserId(userId);
        this.setName(name);
        this.setSurname(surname);
        this.setEmail(userId);
        this.setPassword(new BCryptPasswordEncoder().encode(password));
        this.setRole(role);
        this.setCompany(company);
        this.setPhone(phone);
        this.setCountry(country);
    }

    public User() {
        this("new", "PASSWORD", Role.USER,"","","","","");
    }

    public String getFullName() {
        return this.name + this.surname;
    }
}
