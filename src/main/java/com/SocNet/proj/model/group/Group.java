package com.SocNet.proj.model.group;
import com.SocNet.proj.model.BaseEntity;
import com.SocNet.proj.model.user.User;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "m_group")
@Getter
@Setter
@NoArgsConstructor
public class Group extends BaseEntity {
    @ManyToOne(fetch = FetchType.EAGER)
    private User creator;

    private String name;

    @OneToMany(fetch = FetchType.LAZY)
    private List<User> users;

    public void removeUser(User user){
        this.users.remove(user);
    }

    public void addUser(User user){
        users.add(user);
    }
}
