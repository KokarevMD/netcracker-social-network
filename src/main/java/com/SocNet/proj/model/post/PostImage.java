package com.SocNet.proj.model.post;

import com.SocNet.proj.model.BaseEntity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Type;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class PostImage extends BaseEntity {

    private String url;

    private int sort;

    private boolean isShow = true;

    private String filename;

    private String filetype;

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    private String base64;
}
