package com.SocNet.proj.model.post;

import com.SocNet.proj.model.BaseEntity;
import com.SocNet.proj.model.user.User;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Setter
@Getter
@NoArgsConstructor
@Entity
@Table(name = "m_comment")
public class Comment extends BaseEntity {
    @Column(length = 10000)
    private String message;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "post_id")
    private Post post;

    private Date dateAndTime;
}
