package com.SocNet.proj.model.post;

import com.SocNet.proj.model.BaseEntity;
import com.SocNet.proj.model.user.User;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Entity
@Table(name = "m_post")
@Setter
@Getter
@NoArgsConstructor
public class Post extends BaseEntity {
    @ManyToOne(fetch = FetchType.EAGER)
    User creator;
    LocalDate date;
    String postText;

    @OneToMany(cascade = CascadeType.ALL)
    private List<PostImage> images;

    int likes=0;
    int dislikes=0;
}
