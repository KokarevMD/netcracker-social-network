package com.SocNet.proj.model.session;

import com.SocNet.proj.model.user.User;
import lombok.*;
import java.util.*;

@Getter
@Setter
@NoArgsConstructor
public class SessionItem {
    private String  token;
    private String  userId;
    private String  firstName;
    private String  lastName;
    private String  email;
    private User user;
    private List<String> roles;
}
