package com.SocNet.proj.repo;

import com.SocNet.proj.model.group.Group;
import com.SocNet.proj.model.user.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GroupRepository extends JpaRepository<Group,Long> {
}
