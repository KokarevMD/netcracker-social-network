package com.SocNet.proj.repo;

import com.SocNet.proj.model.post.PostImage;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PostImageRepository extends JpaRepository<PostImage,Long> {
}
