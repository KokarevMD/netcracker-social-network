package com.SocNet.proj.repo;

import com.SocNet.proj.model.post.Comment;
import com.SocNet.proj.model.post.Post;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CommentRepository extends JpaRepository<Comment,Long> {
List<Comment> findAllByPost(Post post);
}
