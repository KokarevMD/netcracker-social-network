package com.SocNet.proj.repo;

import com.SocNet.proj.model.post.Post;
import com.SocNet.proj.model.user.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PostRepository extends JpaRepository<Post,Long> {
    List<Post> findAllByCreator(User user);
}
