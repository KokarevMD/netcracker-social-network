package com.SocNet.proj.repo;

import com.SocNet.proj.model.user.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Integer> {
    Optional<User> findOneByUserId(String userId);
    boolean existsByUserId(String userId);
    Optional<User> findOneByUserIdAndPassword(String userId, String password);
}
