package com.SocNet.proj.service;

import com.SocNet.proj.api.utils.exception.NotFoundException;
import com.SocNet.proj.api.utils.response.OperationResponse;
import com.SocNet.proj.dto.RequestDto;
import com.SocNet.proj.model.user.ProfileImage;
import com.SocNet.proj.model.user.Role;
import com.SocNet.proj.model.user.User;
import com.SocNet.proj.model.user.request.Request;
import com.SocNet.proj.repo.ProfileImageRepository;
import com.SocNet.proj.repo.RequestRepository;
import com.SocNet.proj.repo.UserRepository;
import com.google.common.base.Strings;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserService {

    private final UserRepository userRepository;
    private final ProfileImageRepository profileImageRepository;
    private final RequestRepository requestRepository;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public UserService(UserRepository userRepository, ProfileImageRepository profileImageRepository, RequestRepository requestRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.profileImageRepository = profileImageRepository;
        this.requestRepository = requestRepository;
        this.passwordEncoder = passwordEncoder;
    }

    public User getUserInformation(String userIdParam) {
        User user;
        if (Strings.isNullOrEmpty(userIdParam)) {
            user = getLoggedInUser();
        } else {
            user = getUserInfoByUserId(userIdParam);
        }
        return user;

    }

    public String getLoggedInUserId() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth == null) {
            return "nosession";
        }
        return auth.getName();
    }


    public User getLoggedInUser() {
        String loggedInUserId = this.getLoggedInUserId();
        return this.getUserInfoByUserId(loggedInUserId);
    }

    public User getUserInfoByUserId(String userId) {
        return this.userRepository.findOneByUserId(userId).orElseThrow(()->new NotFoundException("such user didn't exist"));
    }


    private boolean insertOrSaveUser(User user) {
        this.userRepository.save(user);
        return true;
    }

    public boolean addNewUser(User user) {
        user.setPassword(new BCryptPasswordEncoder().encode(user.getPassword()));
            return this.insertOrSaveUser(user);
    }
    public List<User> findAll() {
        return userRepository.findAll();
    }
    public User save(User user) {
        return userRepository.save(user);
    }

    public boolean editUser(User user){
        User userFromRepo = userRepository.findOneByUserId(user.getUserId()).orElseThrow(()->new NotFoundException("user didn't exist"));
        BeanUtils.copyProperties(user,userFromRepo,"password","profileImage");
        return insertOrSaveUser(changeImage(userFromRepo,user.getProfileImage()));
    }

    private User changeImage(User user, ProfileImage profileImage){
        profileImage.setUser(user);
        profileImage = profileImageRepository.save(profileImage);
        user.setProfileImage(profileImage);
        return user;
    }

    public boolean checkPass(String password){
        String userPass = getLoggedInUser().getPassword();
        return passwordEncoder.matches(password,userPass);
    }

    public OperationResponse editPass(String userId,String pass){
        User userFromRepo = userRepository.findOneByUserId(userId).orElseThrow(()->new NotFoundException("user didn't exist"));
        userFromRepo.setPassword(passwordEncoder.encode(pass));
        insertOrSaveUser(userFromRepo);
        return new OperationResponse("pass edited");
    }

    public List<User> getFriends(String userId){
        User userFromRepo= userRepository.findOneByUserId(userId)
                                    .orElseThrow(()->new NotFoundException("user didn't exist"));
        return userFromRepo.getFriends();
    }
    public List<User> getFriendsForPost(String userId){
        User userFromRepo= userRepository.findOneByUserId(userId)
                .orElseThrow(()->new NotFoundException("user didn't exist"));
        return userFromRepo.getFriends();
    }

    public List<User> getAllForFriends(String userId){
        User userFromRepo = userRepository.findOneByUserId(userId).orElseThrow(()->new NotFoundException("user didn't exist"));
        List<User> users= userRepository.findAll();
        return users.stream()
                    .filter(i->isFriends(userFromRepo,i)).filter(i->!i.equals(userFromRepo))
                    .collect(Collectors.toList());
    }

    private boolean isFriends(User user,User friend){
        List<User> requests = requestRepository.findAllBySource(user).stream().map(Request::getTarget).collect(Collectors.toList());
        List<User> friends = user.getFriends();
        return !(friends.contains(friend)||requests.contains(friend));
    }

    public OperationResponse handleRequest(boolean isAccepted, RequestDto request){
        Request requestFromRepo = requestRepository.findById(request.getId()).orElseThrow(()->new NotFoundException("request didn't exist"));
        if(isAccepted){
            User source = userRepository.findOneByUserId(request.getSource().getUserId()).orElseThrow(()->new NotFoundException("user didn't exist"));
            User target = userRepository.findOneByUserId(request.getTarget().getUserId()).orElseThrow(()->new NotFoundException("user didn't exist"));
            source.addFriend(target);
            target.addFriend(source);
        }
        requestRepository.deleteById(requestFromRepo.getId());
        return new OperationResponse("request handled successfully");
    }

    public OperationResponse addRequest(RequestDto request){
        Request newRequest = new Request();
        User source = userRepository.findOneByUserId(request.getSource().getUserId()).orElseThrow(()->new NotFoundException("user didn't exist"));
        User target = userRepository.findOneByUserId(request.getTarget().getUserId()).orElseThrow(()->new NotFoundException("user didn't exist"));
        newRequest.setTarget(target);
        newRequest.setSource(source);
        requestRepository.save(newRequest);
        return new OperationResponse("request added successfully");
    }

    public List<Request> getRequests(String userId){
        User userFromRepo = userRepository.findOneByUserId(userId).orElseThrow(()->new NotFoundException("user didn't exist"));
        return requestRepository.findAllByTarget(userFromRepo);
    }

    public OperationResponse deleteFriend(String userId,User friend){
        User userFromRepo = userRepository.findOneByUserId(userId).orElseThrow(()->new NotFoundException("user didn't exist"));
        User friendFromRepo = userRepository.findOneByUserId(friend.getUserId()).orElseThrow(()->new NotFoundException("user didn't exist"));
        userFromRepo.setFriends(userFromRepo.delFriend(friendFromRepo));
        friendFromRepo.setFriends(friendFromRepo.delFriend(userFromRepo));
        insertOrSaveUser(userFromRepo);
        insertOrSaveUser(friendFromRepo);
        return new OperationResponse("friend deleted");
    }
}
