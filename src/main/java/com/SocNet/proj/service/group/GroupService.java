package com.SocNet.proj.service.group;

import com.SocNet.proj.api.utils.exception.NotFoundException;
import com.SocNet.proj.api.utils.response.OperationResponse;
import com.SocNet.proj.model.group.Group;
import com.SocNet.proj.model.user.User;
import com.SocNet.proj.repo.GroupRepository;
import com.SocNet.proj.repo.UserRepository;
import com.SocNet.proj.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class GroupService {
    private final GroupRepository groupRepository;
    private final UserRepository userRepository;
    private final UserService userService;

    @Autowired
    public GroupService(GroupRepository groupRepository, UserRepository userRepository, UserService userService) {
        this.groupRepository = groupRepository;
        this.userRepository = userRepository;
        this.userService = userService;
    }


    public OperationResponse addGroup(Group group) {
        if (Objects.nonNull(group)) {
            group.setCreator(userService.getLoggedInUser());
            groupRepository.save(group);
        } else throw new NotFoundException("Unable to add group");
        return new OperationResponse("group added");
    }

    public OperationResponse deleteGroup(Long groupId) {
        Group group=groupRepository.findById(groupId).orElseThrow(()->new NotFoundException("there is no group"));
        if(userService.getLoggedInUser().equals(group.getCreator())){
            groupRepository.deleteById(groupId);
            return new OperationResponse("group deleted");
        }else throw new AccessDeniedException("you are not group creator");
    }

    public OperationResponse joinGroup(Long groupId){
        Group group=groupRepository.findById(groupId).orElseThrow(()->new NotFoundException("there is no group"));
        User user=userRepository.findOneByUserId(userService.getLoggedInUserId()).orElseThrow(()->new NotFoundException("there is no user"));
        group.addUser(user);
        groupRepository.save(group);
        return new OperationResponse("user successfully join group");
    }

    public OperationResponse leaveGroup(Long groupId){
        Group group=groupRepository.findById(groupId).orElseThrow(()->new NotFoundException("there is no group"));
        User user=userRepository.findOneByUserId(userService.getLoggedInUserId()).orElseThrow(()->new NotFoundException("there is no user"));
        group.removeUser(user);
        groupRepository.save(group);
        return new OperationResponse("user successfully leave group");
    }

    public List<Group> getGroupsForUser(String userId){
        User user=userRepository.findOneByUserId(userId).orElseThrow(()->new NotFoundException("there is no user"));
        return groupRepository.findAll()
                .stream()
                .filter(group -> group.getUsers().contains(user)||group.getCreator().equals(user))
                .collect(Collectors.toList());
    }

    public List<Group> getGroupsForJoin(String userId){
        User user=userRepository.findOneByUserId(userId).orElseThrow(()->new NotFoundException("there is no user"));
        return groupRepository.findAll()
                .stream()
                .filter(group -> !group.getUsers().contains(user))
                .filter(group -> !group.getCreator().equals(user))
                .collect(Collectors.toList());
    }
}
