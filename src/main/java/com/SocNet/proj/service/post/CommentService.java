package com.SocNet.proj.service.post;

import com.SocNet.proj.api.utils.exception.NotFoundException;
import com.SocNet.proj.dto.UserDto;
import com.SocNet.proj.dto.post.CommentDto;
import com.SocNet.proj.model.post.Comment;
import com.SocNet.proj.model.post.Post;
import com.SocNet.proj.model.user.User;
import com.SocNet.proj.repo.CommentRepository;
import com.SocNet.proj.repo.PostRepository;
import com.SocNet.proj.repo.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

@Service
public class CommentService {

    private final PostRepository postRepository;
    private final CommentRepository commentRepository;
    private final UserRepository userRepository;

    @Autowired
    public CommentService(PostRepository postRepository, CommentRepository commentRepository, UserRepository userRepository) {
        this.postRepository = postRepository;
        this.commentRepository = commentRepository;
        this.userRepository = userRepository;
    }

    public List<Comment> getAllmessagesByPost(Long postId) {
        Post post = postRepository.findById(postId).orElseThrow(() -> new NotFoundException("There is no such post"));
        List <Comment> messages = new ArrayList<>();
        return commentRepository.findAllByPost(post);
    }

    public CommentDto addMessagesByPost(CommentDto dto) {
        if(Objects.isNull(dto.getUser().getUserId()) || Objects.isNull(dto.getPostId())) { return null; }
        User user = userRepository.findOneByUserId(dto.getUser().getUserId()).orElseThrow(() -> new NotFoundException("There is no such user"));
        Post post = postRepository.findById(dto.getPostId()).orElseThrow(() -> new NotFoundException("There is no such post"));

        Comment comment = CommentDto.convertToEntity(dto, post, user);
        assert comment != null;
        return CommentDto.convertFromEntity(commentRepository.save(comment), UserDto.convertFromEntity(user), post.getId());
    }
}
