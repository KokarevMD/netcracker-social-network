package com.SocNet.proj.service.post;

import com.SocNet.proj.api.utils.exception.InvalidArgumentException;
import com.SocNet.proj.api.utils.exception.NotFoundException;
import com.SocNet.proj.api.utils.response.OperationResponse;
import com.SocNet.proj.dto.post.PostDto;
import com.SocNet.proj.model.post.Post;
import com.SocNet.proj.model.post.PostImage;
import com.SocNet.proj.model.user.User;
import com.SocNet.proj.repo.PostImageRepository;
import com.SocNet.proj.repo.PostRepository;
import com.SocNet.proj.repo.UserRepository;
import com.SocNet.proj.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class PostService {
    private final PostRepository postRepository;
    private final UserService userService;
    private final UserRepository userRepository;
    private final PostImageRepository postImageRepository;
    @Autowired
    public PostService(PostRepository postRepository, UserService userService, UserRepository userRepository, PostImageRepository postImageRepository) {
        this.postRepository = postRepository;
        this.userService = userService;
        this.userRepository = userRepository;
        this.postImageRepository = postImageRepository;
    }

    public Post getPost(Long id){
        return postRepository.findById(id).orElseThrow(()->new NotFoundException("there is no post"));
    }

    public List<PostDto> getAllPosts(){
        return postRepository.findAll().stream().map(PostDto::convertFromEntity).collect(Collectors.toList());
    }

    public List<Post> getAllFriendPosts(String userId){
        List<Post> posts = postRepository.findAllByCreator(userRepository.findOneByUserId(userId).orElseThrow(() -> new NotFoundException("there is no user")));
        List<User> friends = userService.getFriendsForPost(userId);
        friends.forEach(friend->posts.addAll(postRepository.findAllByCreator(friend)));
        return posts;
    }

    public OperationResponse addPost(Post post){
        if(Objects.nonNull(post)){
            post.setCreator(userService.getLoggedInUser());
            post.setDate(LocalDate.now());
            List<PostImage> images=post.getImages();
            post.setImages(new ArrayList<>());
            post = postRepository.save(post);
            post.setImages(images);
            postRepository.save(post);
            return new OperationResponse(String.valueOf(post.getId()));
        } else{
            throw new InvalidArgumentException("post is empty");
        }
    }

    public OperationResponse deletePost(Long id){
        Post postFromRepo = postRepository.findById(id).orElseThrow(()->new NotFoundException("there is no post"));
        if(userService.getLoggedInUser().equals(postFromRepo.getCreator())){
            postRepository.deleteById(id);
            return new OperationResponse("post deleted");
        }else throw new AccessDeniedException("you are not post creator");
    }

    public OperationResponse editPost(Long id, Post post){
        if(Objects.nonNull(post)){
            Post postFromRepo = postRepository.findById(id).orElseThrow(()->new NotFoundException("there is no post"));
            postFromRepo.setCreator(post.getCreator());
            postFromRepo.setDate(post.getDate());
            postFromRepo.setPostText(post.getPostText());
            postFromRepo.setImages(post.getImages());
            postRepository.save(postFromRepo);
            return new OperationResponse("post edited");
        } else {
            throw new InvalidArgumentException("post is empty");
        }
    }

    public OperationResponse addLike(Long postId){
        Post post = postRepository.findById(postId).orElseThrow(()->new NotFoundException("there is no such post"));
        post.setLikes(post.getLikes()+1);
        postRepository.save(post);
        return new OperationResponse("like added");
    }
    public OperationResponse removeLike(Long postId){
        Post post = postRepository.findById(postId).orElseThrow(()->new NotFoundException("there is no such post"));
        post.setLikes(post.getLikes()-1);
        postRepository.save(post);
        return new OperationResponse("like removed");
    }

    public OperationResponse addDislike(Long postId){
        Post post = postRepository.findById(postId).orElseThrow(()->new NotFoundException("there is no such post"));
        post.setDislikes(post.getDislikes()+1);
        postRepository.save(post);
        return new OperationResponse("dislike added");
    }
    public OperationResponse removeDislike(Long postId){
        Post post = postRepository.findById(postId).orElseThrow(()->new NotFoundException("there is no such post"));
        post.setDislikes(post.getDislikes()-1);
        postRepository.save(post);
        return new OperationResponse("dislike removed");
    }
}
