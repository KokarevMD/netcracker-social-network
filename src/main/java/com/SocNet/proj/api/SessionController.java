package com.SocNet.proj.api;

import com.SocNet.proj.api.utils.exception.NotFoundException;
import com.SocNet.proj.model.session.SessionItem;
import com.SocNet.proj.model.user.Login;
import com.SocNet.proj.model.user.User;
import com.SocNet.proj.repo.UserRepository;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/*
This is a dummy rest controller, for the purpose of documentation (/session) path is map to a filter
 - This will only be invoked if security is disabled
 - If Security is enabled then SessionFilter.java is invoked
 - Enabling and Disabling Security is done at config/applicaton.properties 'security.ignored=/**'
*/

@RestController
@Api(tags = {"Authentication"})
public class SessionController {

    private final UserRepository userRepository;

    @Autowired
    public SessionController(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @ApiResponses(value = {@ApiResponse(code = 200, message = "Will return a security token, which must be passed in every request")})
    @RequestMapping(value = "/session", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public SessionItem newSession(@RequestBody Login login, HttpServletRequest request, HttpServletResponse response) {
        User user = userRepository.findOneByUserIdAndPassword(login.getUsername(), login.getPassword()).orElse(null);
        SessionItem sessionItem = new SessionItem();
        if (user != null) {
            sessionItem.setToken("xxx.xxx.xxx");
            sessionItem.setUserId(user.getUserId());
            sessionItem.setFirstName(user.getName());
            sessionItem.setLastName(user.getSurname());
            sessionItem.setEmail(user.getEmail());
            return sessionItem;
        } else {
            throw new NotFoundException("Login Failed");
        }
    }

}
