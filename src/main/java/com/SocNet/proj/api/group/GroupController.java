package com.SocNet.proj.api.group;

import com.SocNet.proj.api.utils.response.OperationResponse;
import com.SocNet.proj.dto.group.GroupDto;
import com.SocNet.proj.model.group.Group;
import com.SocNet.proj.service.group.GroupService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping(value = "/api/groups", produces = MediaType.APPLICATION_JSON_VALUE)
@Api(tags = {"Groups"})
public class GroupController {

    private final GroupService groupService;

    @Autowired
    public GroupController(GroupService groupService) {
        this.groupService = groupService;
    }

    // ------------------Add New Group---------------------------------------------
    @ApiOperation(value = "Add new group", response = OperationResponse.class)
    @RequestMapping(value = "/add", method = RequestMethod.POST, produces = {"application/json"})
    public OperationResponse addNewGroup(@RequestBody Group group) {
        return groupService.addGroup(group);
    }

    //-------------------Delete A Group----------------------------------------------
    @ApiOperation(value = "Delete a course", response = OperationResponse.class)
    @RequestMapping(value = "/del/{groupId}", method = RequestMethod.DELETE, produces = {"application/json"})
    public OperationResponse deleteProduct(@PathVariable("groupId") Long groupId, HttpServletRequest req) {
        return groupService.deleteGroup(groupId);
    }

    //-------------------Get all user groups----------------------------------------
    @ApiOperation(value = "Get all user groups", response = OperationResponse.class)
    @RequestMapping(value="/all/{userId}", method = RequestMethod.GET, produces = {"application/json"})
    public List<GroupDto> groupsForUser(@PathVariable("userId")String userId){
        return GroupDto.convertFromEntities(groupService.getGroupsForUser(userId));
    }

    //-------------------Get all groups for join----------------------------------------
    @ApiOperation(value = "Get all groups for join", response = OperationResponse.class)
    @RequestMapping(value="/all/join/{userId}", method = RequestMethod.GET, produces = {"application/json"})
    public List<GroupDto> groupsForJoin(@PathVariable("userId")String userId){
        return GroupDto.convertFromEntities(groupService.getGroupsForJoin(userId));
    }


    //-------------------Join group----------------------------------------
    @ApiOperation(value = "Join group", response = OperationResponse.class)
    @RequestMapping(value="/join/{groupId}", method = RequestMethod.POST, produces = {"application/json"})
    public OperationResponse joinGroup(@PathVariable("groupId")Long groupId){
        return groupService.joinGroup(groupId);
    }

    //-------------------Leave group----------------------------------------
    @ApiOperation(value = "Leave group", response = OperationResponse.class)
    @RequestMapping(value="/leave/{groupId}", method = RequestMethod.POST, produces = {"application/json"})
    public OperationResponse leaveGroup(@PathVariable("groupId")Long groupId){
        return groupService.leaveGroup(groupId);
    }

}
