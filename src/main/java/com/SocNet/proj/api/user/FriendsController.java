package com.SocNet.proj.api.user;

import com.SocNet.proj.api.utils.response.OperationResponse;
import com.SocNet.proj.dto.RequestDto;
import com.SocNet.proj.dto.UserDto;
import com.SocNet.proj.model.user.User;
import com.SocNet.proj.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping(value = "/friends", produces = MediaType.APPLICATION_JSON_VALUE)
@Api(tags = {"Authentication"})
public class FriendsController {
    private final UserService userService;

    @Autowired
    public FriendsController(UserService userService) {
        this.userService = userService;
    }

    //------------------Get friends---------------------------------------------
    @ApiOperation(value = "Get friends", response = OperationResponse.class)
    @RequestMapping(value = "/{userId}", method = RequestMethod.GET, produces = {"application/json"})
    public List<UserDto> getFriends(@PathVariable("userId") String userId, HttpServletRequest req) {
        return UserDto.convertFromEntities(userService.getFriends(userId));
    }

    //------------------Get friend candidates---------------------------------------------
    @ApiOperation(value = "Get friend candidates", response = OperationResponse.class)
    @RequestMapping(value = "/all/{userId}", method = RequestMethod.GET, produces = {"application/json"})
    public List<UserDto> getFriendCandidates(@PathVariable("userId") String userId, HttpServletRequest req) {
        return UserDto.convertFromEntities(userService.getAllForFriends(userId));
    }

    //------------------Add friend request---------------------------------------------
    @ApiOperation(value = "Add friend request", response = OperationResponse.class)
    @RequestMapping(value = "/addRequest", method = RequestMethod.POST, produces = {"application/json"})
    public OperationResponse addRequest(@RequestBody RequestDto dto, HttpServletRequest req) {
        return userService.addRequest(dto);
    }

    //------------------handle request---------------------------------------------
    @ApiOperation(value = "handle request", response = OperationResponse.class)
    @RequestMapping(value = "/handle/{isAccepted}", method = RequestMethod.POST, produces = {"application/json"})
    public OperationResponse handleReq(@RequestBody RequestDto dto, @PathVariable("isAccepted") boolean isAccepted, HttpServletRequest req) {
        return userService.handleRequest(isAccepted,dto);
    }

    //------------------Get requests---------------------------------------------
    @ApiOperation(value = "Get friend candidates", response = OperationResponse.class)
    @RequestMapping(value = "/req/all/{userId}", method = RequestMethod.GET, produces = {"application/json"})
    public List<RequestDto> getRequests(@PathVariable("userId") String userId, HttpServletRequest req) {
        return RequestDto.convertFromEntities(userService.getRequests(userId));
    }

    //------------------Delete friend---------------------------------------------
    @ApiOperation(value = "Delete friend", response = OperationResponse.class)
    @RequestMapping(value = "/del/{userId}", method = RequestMethod.POST, produces = {"application/json"})
    public OperationResponse getRequests(@PathVariable("userId") String userId,@RequestBody User friend, HttpServletRequest req) {
        return userService.deleteFriend(userId,friend);
    }
}
