package com.SocNet.proj.api.user;

import com.SocNet.proj.api.utils.response.OperationResponse;
import com.SocNet.proj.dto.UserDto;
import com.SocNet.proj.model.user.User;
import com.SocNet.proj.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping(value = "/user", produces = MediaType.APPLICATION_JSON_VALUE)
@Api(tags = {"Authentication"})
public class UserController {
    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @ApiOperation(value = "Get current user information")
    @RequestMapping(value = "/get", method = RequestMethod.GET, produces = {"application/json"})
    public UserDto getUserInformation(@RequestParam(value = "name", required = false) String userIdParam, HttpServletRequest req) {
        return UserDto.convertFromEntity(userService.getUserInformation(userIdParam));
    }

    // -------------------Get all Users---------------------------------------------
    @ApiOperation(value = "get all users")
    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public List<UserDto> getAllUsers() {
        return UserDto.convertFromEntities(userService.findAll());
    }

    //------------------Add new user---------------------------------------------
    @ApiOperation(value = "Add new user", response = OperationResponse.class)
    @RequestMapping(value = "/registration", method = RequestMethod.POST, produces = {"application/json"})
    public OperationResponse addNewUser(@RequestBody User user, HttpServletRequest req) {
        boolean userAddSuccess = userService.addNewUser(user);
        return userAddSuccess ? new OperationResponse("user added") : new OperationResponse("user did not added");
    }

    //------------------Edit user---------------------------------------------
    @ApiOperation(value = "Edit user", response = OperationResponse.class)
    @RequestMapping(value = "/edit", method = RequestMethod.POST, produces = {"application/json"})
    public OperationResponse editUser(@RequestBody User user, HttpServletRequest req) {
        boolean userAddSuccess = userService.editUser(user);
        return userAddSuccess ? new OperationResponse("user edited") : new OperationResponse("user did not edited");
    }
    //------------------Check password---------------------------------------------
    @ApiOperation(value = "Check password", response = OperationResponse.class)
    @RequestMapping(value = "/check", method = RequestMethod.POST, produces = {"application/json"})
    public boolean checkPass(@RequestBody String pass, HttpServletRequest req) {
        return userService.checkPass(pass.substring(1,pass.length()-1));
    }
    //------------------Edit user pass---------------------------------------------
    @ApiOperation(value = "Edit user pass", response = OperationResponse.class)
    @RequestMapping(value = "/edit/pass/{userId}", method = RequestMethod.POST, produces = {"application/json"})
    public OperationResponse editUserPass(@RequestBody String pass,@PathVariable("userId") String userId, HttpServletRequest req) {
        return userService.editPass(userId,pass.substring(1,pass.length()-1));
    }

}
