package com.SocNet.proj.api.post;


import com.SocNet.proj.api.utils.response.OperationResponse;
import com.SocNet.proj.dto.post.PostDto;
import com.SocNet.proj.model.post.Post;
import com.SocNet.proj.service.post.PostService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/api/post", produces = MediaType.APPLICATION_JSON_VALUE)
@Api(tags = {"Post"})
public class PostController {
    private final PostService postService;

    @Autowired
    public PostController(PostService postService) {
        this.postService = postService;
    }

    // -------------------Get all friends posts---------------------------------------------
    @ApiOperation(value = "Get all friends posts")
    @RequestMapping(value = "/all/{userId}", method = RequestMethod.GET, produces = {"application/json"})
    public List<PostDto> getPosts(@PathVariable("userId") String userId) {
        return postService.getAllFriendPosts(userId)
                .stream()
                .map(PostDto::convertFromEntity)
                .collect(Collectors.toList());
    }

    // -------------------Retrieve One Post---------------------------------------------
    @ApiOperation(value = "One post")
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public PostDto getOnePost(@PathVariable("id") Long id) {
        return PostDto.convertFromEntity(postService.getPost(id));
    }


    // -------------------add Post---------------------------------------------
    @ApiOperation(value = "add post")
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public OperationResponse addPost(@RequestBody Post post) {
        return postService.addPost(post);
    }

    // -------------------add like---------------------------------------------
    @ApiOperation(value = "add like")
    @RequestMapping(value = "/like/add/{postId}", method = RequestMethod.GET)
    public OperationResponse addLike(@PathVariable("postId") Long postId) {
        return postService.addLike(postId);
    }

    // -------------------remove like---------------------------------------------
    @ApiOperation(value = "remove like")
    @RequestMapping(value = "/like/remove/{postId}", method = RequestMethod.GET)
    public OperationResponse removeLike(@PathVariable("postId") Long postId) {
        return postService.removeLike(postId);
    }

    // -------------------add dislike---------------------------------------------
    @ApiOperation(value = "add dislike")
    @RequestMapping(value = "/dislike/add/{postId}", method = RequestMethod.GET)
    public OperationResponse addDislike(@PathVariable("postId") Long postId) {
        return postService.addDislike(postId);
    }

    // -------------------remove dislike---------------------------------------------
    @ApiOperation(value = "remove dislike")
    @RequestMapping(value = "/dislike/remove/{postId}", method = RequestMethod.GET)
    public OperationResponse removeDislike(@PathVariable("postId") Long postId) {
        return postService.removeDislike(postId);
    }
}
