package com.SocNet.proj.api.post;

import com.SocNet.proj.api.utils.exception.NotFoundException;
import com.SocNet.proj.dto.UserDto;
import com.SocNet.proj.dto.post.CommentDto;
import com.SocNet.proj.model.post.Post;
import com.SocNet.proj.repo.PostRepository;
import com.SocNet.proj.service.post.CommentService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

@RestController
@RequestMapping(value = "/api/comment", produces = MediaType.APPLICATION_JSON_VALUE)
@Api(tags = {"Comment"})
public class CommentController {

private final CommentService commentService;
private final PostRepository postRepository;

    @Autowired
    public CommentController(CommentService commentService, PostRepository postRepository) {
        this.commentService = commentService;
        this.postRepository = postRepository;
    }

    //-------------------Add new comment----------------------------------------------------
    @ApiOperation(value = "add new message discussion by Post")
    @RequestMapping(value = "/add", method = RequestMethod.POST,produces = {"application/json"})
    public CommentDto addMessageDiscussionByCourse(@RequestBody CommentDto dto) {
        return commentService.addMessagesByPost(dto);
    }

    //----------------------------get all comments----------------------------------------------------
    @ApiOperation(value = "get all comments")
    @RequestMapping(value = "/all/{postId}", method = RequestMethod.GET)
    public List<CommentDto> getAllmessages(@PathVariable("postId") Long postId) {
        List <CommentDto> messages = new ArrayList<>();
        Post post = postRepository.findById(postId).orElseThrow(() -> new NotFoundException("There is no such post"));
        commentService.getAllmessagesByPost(postId).forEach(mes -> {
            UserDto userDto = UserDto.convertFromEntity(mes.getUser());
            CommentDto dto = CommentDto.convertFromEntity(mes, userDto, post.getId());
            assert dto != null;
            messages.add(dto);
        });
        List <CommentDto> list = new ArrayList<>();
        messages.stream().sorted(Comparator.comparing(CommentDto::getDateAndTime).reversed()).forEach(list::add);
        return list;
    }

}
